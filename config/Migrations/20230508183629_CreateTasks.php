<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateTasks extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('tasks', ['id' => true, 'primary_key' => ['id']]);
        $table->addColumn('task_name', 'string', [
                    'limit' => 50,
                    'null' => false,
                ])
              ->addColumn('description', 'text', [
                    'default' => null,
                ])
              ->addColumn('status', 'string', [
                    'default' => null,
                    'limit' => 10
                ])
              ->addColumn('created', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('updated', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('deleted', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addIndex(['task_name'])
              ->create();

        if ($this->isMigratingUp()) {
          $table->insert([
                    [
                      'task_name' => 'ADMIN', 
                      'description' => 'Mengerjakan proses administrasi',
                      'status' => '1'
                    ],
                    [
                      'task_name' => 'PROG', 
                      'description' => 'Mengerjakan pemrograman',
                      'status' => '1'
                    ],
                    [
                      'task_name' => 'OB', 
                      'description' => 'Bagian kebersihan dan membuat kopi',
                      'status' => '1'
                    ],
                    [
                      'task_name' => 'DESIGN', 
                      'description' => 'Membuat dan merancang design',
                      'status' => '0'
                    ]
                  ])
                ->save();
        }
    }
}
