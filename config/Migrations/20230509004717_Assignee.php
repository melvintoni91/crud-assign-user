<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Assignee extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('assignees', ['id' => true, 'primary_key' => ['id']]);
        $table->addColumn('task_name', 'string', [
                    'limit' => 50,
                    'null' => false,
                ])
              ->addColumn('user_id', 'integer', [
                    'null' => false,
                ])
              ->addColumn('created', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('updated', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('deleted', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addForeignKey('task_name', 'tasks', 'task_name')
              ->addForeignKey('user_id', 'users', 'id')
              ->create();

        if ($this->isMigratingUp()) {
          $table->insert([
                    [
                        'task_name' => 'ADMIN', 
                        'user_id' => 1
                    ],
                    [
                        'task_name' => 'PROG', 
                        'user_id' => 2
                    ],
                    [
                        'task_name' => 'OB', 
                        'user_id' => 3
                    ],
                    [
                        'task_name' => 'DESIGN', 
                        'user_id' => 4
                    ]
                  ])
                ->save();
        }
    }
}
