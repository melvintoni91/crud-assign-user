<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('users', ['id' => true, 'primary_key' => ['id']]);
        $table->addColumn('name', 'string', [
                    'limit' => 50,
                    'null' => false,
                ])
              ->addColumn('address', 'text', [
                    'default' => null,
                ])
              ->addColumn('phone', 'string', [
                    'default' => null,
                    'limit' => 15
                ])
              ->addColumn('email', 'string', [
                    'default' => null,
                    'limit' => 50
                ])
              ->addColumn('status', 'string', [
                    'default' => null,
                    'limit' => 10
                ])
              ->addColumn('created', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('updated', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->addColumn('deleted', 'datetime', [
                    'default' => date('Y-m-d H:i:s')
                ])
              ->create();

        if ($this->isMigratingUp()) {
          $table->insert([
                    [
                      'name' => 'Agung Azali', 
                      'address' => 'Jl. Kebanggaan No.10', 
                      'phone' => '628763738833',
                      'email' => 'agung@email.com',
                      'status' => '1',
                    ],
                    [
                      'name' => 'Budi Bimantara', 
                      'address' => 'Jl. Kebahagiaan No.11', 
                      'phone' => '6287137777432',
                      'email' => 'budi@email.com',
                      'status' => '1',
                    ],
                    [
                      'name' => 'Charlie Cimol', 
                      'address' => 'Jl. Kesukaan No.12', 
                      'phone' => '6286578483322',
                      'email' => 'charlie@email.com',
                      'status' => '1',
                    ],
                    [
                      'name' => 'Dede Dukuh', 
                      'address' => 'Jl. Kemuliaan No.13', 
                      'phone' => '6286578443321',
                      'email' => 'dede@email.com',
                      'status' => '0',
                    ]
                  ])
                ->save();
        }
    }
}
