<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Assignees Controller
 *
 * @property \App\Model\Table\AssigneesTable $Assignees
 * @method \App\Model\Entity\Assignee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssigneesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $assignees = $this->paginate($this->Assignees);

        $this->set(compact('assignees'));
    }

    /**
     * View method
     *
     * @param string|null $id Assignee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assignee = $this->Assignees->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set(compact('assignee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $tasknames = $this->request->getData('task_name');
            $arr = [];
            foreach ($tasknames as $tn) {
                $arr[] = $tn;
            }
            
            $assignee = $this->Assignees->newEmptyEntity();
            $assignee = $this->Assignees->patchEntity($assignee, $this->request->getData());
            $assignee->task_name = $arr;
            $chkErr = 0;
            foreach ($arr as $a) {
                $assignee = $this->Assignees->newEmptyEntity();
                $assignee->user_id = $this->request->getData('user_id');
                $assignee->task_name = $a;
                
                if ($this->Assignees->save($assignee)) {
                    $chkErr += 1;
                } else {
                    $chkErr -= 1;
                }
            }

            if (count($arr) == $chkErr) {
                $this->Flash->success(__('The assignee has been saved.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The assignee could not be saved. Please, try again.'));
            }
        }
        $users = $this->Assignees->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('assignee', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Assignee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assignee = $this->Assignees->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assignee = $this->Assignees->patchEntity($assignee, $this->request->getData());
            if ($this->Assignees->save($assignee)) {
                $this->Flash->success(__('The assignee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The assignee could not be saved. Please, try again.'));
        }
        $users = $this->Assignees->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('assignee', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Assignee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assignee = $this->Assignees->get($id);
        if ($this->Assignees->delete($assignee)) {
            $this->Flash->success(__('The assignee has been deleted.'));
        } else {
            $this->Flash->error(__('The assignee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
