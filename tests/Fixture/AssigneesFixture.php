<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AssigneesFixture
 */
class AssigneesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'task_name' => 'Lorem ipsum dolor sit amet',
                'user_id' => 1,
                'created' => '2023-05-09 03:07:55',
                'updated' => '2023-05-09 03:07:55',
                'deleted' => '2023-05-09 03:07:55',
            ],
        ];
        parent::init();
    }
}
