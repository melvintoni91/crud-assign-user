<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignee $assignee
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Assignees'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="assignees form content">
            <?= $this->Form->create($assignee) ?>
            <fieldset>
                <legend><?= __('Add Assignee') ?></legend>
                <?php
                    echo $this->Form->control('task_name');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('deleted');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
