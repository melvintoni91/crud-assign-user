<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignee $assignee
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Assignee'), ['action' => 'edit', $assignee->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Assignee'), ['action' => 'delete', $assignee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignee->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Assignees'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Assignee'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="assignees view content">
            <h3><?= h($assignee->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Task Name') ?></th>
                    <td><?= h($assignee->task_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $assignee->has('user') ? $this->Html->link($assignee->user->name, ['controller' => 'Users', 'action' => 'view', $assignee->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($assignee->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($assignee->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated') ?></th>
                    <td><?= h($assignee->updated) ?></td>
                </tr>
                <tr>
                    <th><?= __('Deleted') ?></th>
                    <td><?= h($assignee->deleted) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
