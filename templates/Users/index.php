<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 */
?>
<div class="users index content">
    <?= $this->Html->link(__('New User'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Users') ?></h3>
    <div class="table-responsive">
    
        <div class="modal fade" id="assignModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content row g-3">
                    <div class="modal-header">
                        <h2 class="modal-title fs-5">Assign Task</h2>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <?= $this->Form->create(null, array('url' => array('controller' => 'Assignees', 'action' => 'add'))); ?>

                    <div class="modal-body">
                        <input type="hidden" name="user_id" id="userid" value="">

                        <div class="col-auto">
                            <input type="text" name="task_name[]" id="taskname" placeholder="Insert task name">
                        </div>
                        <div class="col-auto">
                            <button type="button" id="addBtn" class="mb-1 big-txt">+</button>
                            <button type="button" id="substractBtn" class="mb-1 big-txt">-</button>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <?= $this->Form->button(__('Submit')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>


        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('phone') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Number->format($user->id) ?></td>
                    <td><?= h($user->name) ?></td>
                    <td><?= h($user->phone) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                        <button type="button" class="btn btn-primary open-modal" data-bs-toggle="modal" data-bs-target="#assignModal" <?php echo $user->status == '0' ? 'disabled' : ''; ?>> 
                            Assign Task
                        </button>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        $('.open-modal').click(function() {
            var $row = $(this).closest("tr");
            var $tds = $row.find("td:nth-child(1)");
            $.each($tds, function() {
                $('input#userid').val($(this).text());
            });
        });

        var counter = 1;
        $('#addBtn').click(function() {
            counter++;
            var newTextboxId = 'taskname' + counter;
            var newTextbox = $('<input>').attr({
                type: 'text',
                name: 'task_name[]',
                id: newTextboxId
            });
            $(newTextbox).insertBefore('#addBtn');
        });

        $('#substractBtn').click(function() {
            var textboxes = $('input[name="task_name[]"]');
            if (textboxes.length > 1) {
                var lastTextbox = textboxes.last();
                $(lastTextbox).remove();
                counter--;
            }
        });


        let inputTxtName = '#taskname';
        $(inputTxtName).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: '<?php echo $this->Url->build(["controller" => "Tasks", "action" => "autocomplete"]); ?>',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    success: function(data) {
                        var values = Object.values(data);
                        $(inputTxtName).val(values)
                    }
                });
            },
            minLength: 2
        });

    });
</script>